// 1. Import necessary packages
import { ENDPOINT, UAT_TOKEN } from '../test-data/test-data.js'
import { createCourse, retrieveCourse, removeCourse } from '../utilities/courseService.js'
import { Rate } from 'k6/metrics'

const failedRate = new Rate('Custom Failed Requests')
const responseTimeRequest = new Rate('Response Time Request')

// 2. Export options
export let options = {
    vus: 20,
    stages: [
        { duration: '20s', target: 20 }, // Ramp-up from 0 to 20 VUs in 20 seconds
        { duration: '10s', target: 20 }, // Stay at the current load in 10 seconds
        { duration: '10s', target: 40 }, // Ramp-up from 20 to 50 VUs
        { duration: '10s', target: 0 } // Ramp-down from 50 to 0 VU
    ],
    rps: 5
}

// 3. Init environment
export default function () {
    // Main flow: Create course > Retrieve course > Remove course
    // 1. Create new course and return courseId
    let courseId = createCourse(ENDPOINT, UAT_TOKEN)
    console.log(`New course created successfully - ${courseId}`)

    // 2. Retrieve newly created course
    console.log(`Endpoint used - ${ENDPOINT}; UAT Token - ${UAT_TOKEN}; Course ID - ${courseId}`)
    let retrieveResponse = retrieveCourse(ENDPOINT, UAT_TOKEN, courseId)
    if (retrieveResponse[0]) {
        console.log(`Course is retrived successfully - ${retrieveResponse[1]}`)
    }

    // 3. Remove successfully
    let removeResponse = removeCourse(ENDPOINT, UAT_TOKEN, courseId)
    if (removeResponse[0]) {
        console.log(`Course is removed successfully - ${removeResponse[1]}`)
    }
} 